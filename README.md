# Boatmurdered

### Description
Boatmurdered is the story of an infamous Dwarf Fortress succession game that
originated on Something Awful. Highlights include elephantine genocidal warfare
against incessantly moronic-greedy dwarfs and "Fuck the World" levers that drown
everything outside of the fortress in an ocean of all-consuming magma. There
really is no good way to convey the insanity beyond reading the story itself.
This is THE gametale of all time and set the tone of 99% of them. 

### Usage
Currently, you will need [Lyx](https://www.lyx.org/) to access the core document
but there is also a PDF version if all you're looking to do is read the tale.

### Roadmap
 - Clean up the table of contents to match part and section names
 - Harmonize formatting across the entire document
 - Put images into subfolders instead of all in the root
 - Crop and format all images to fit better inline.
 - Convert to native LaTeX
 - Offer eReader formats (ePub, mobi, etc)

### Contributing
 - Create a personal fork of the project on GitLab.
 - Clone the fork on your local machine.
 - Add the original repository as a remote called upstream.
 - If you created your fork a while ago be sure to pull upstream changes into
     your local repository.
 - Create a new branch to work on!
 - Implement/fix your feature, comment your code.
 - Follow the code style of the project, including indentation.
 - Add or change the documentation as needed.
 - Squash your commits into a single commit with git's interactive rebase.
     Create a new branch if necessary.
 - Push your branch to your fork on GitLab, the remote origin.
 - From your fork open a pull request in the correct branch.
 - Once the pull request is approved and merged you can pull the changes from
     upstream to your local repo and delete your extra branch(es).
 - Always write your commit messages in the present tense.

### Authors and acknowledgment
Thank you to the original players/contributors to this epic tale.  Without your
  bravery, this project wouldn't need to exist.

### License
This repository is GPLv3 licensed.  This only covers the formatting and final
  PDF.  The content and author's contributions are their own.